import React from "react";

function asyncLoadZone() {
  var ins = document.createElement("ins");
  ins.dataset.reviveZoneid = "960";
  ins.dataset.reviveId = "c12c3786102a52387cae4913554fc52f";
  ins.dataset.reviveProductid = "PRODUCTID";
  document.body.append(ins);
  var script = document.createElement("script");
  script.src = "//rev.owltrack.com/d/asyncjs.php";
  document.body.append(script);
}

function App() {
  return (
    <div>
      <button onClick={asyncLoadZone}>Click to load script</button>
      <p>
        See source (ctrl+u) or visit our{" "}
        <a
          href="https://bitbucket.org/inis-pl/inis-script-loading/src/master/product/async/react/src/App.js"
          target="_blank"
          rel="noopener noreferrer"
        >
          repository
        </a>
      </p>
    </div>
  );
}

export default App;
